package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    public String attack(){
        return "Attacking with Gun : POW POW!";
    }

    public String getType(){
        return "gun";
    }
}
