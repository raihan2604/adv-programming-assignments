package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    public String attack(){
        return "Attacking with Magic : CHEW CHEW!";
    }

    public String getType(){
        return "magic";
    }
}
