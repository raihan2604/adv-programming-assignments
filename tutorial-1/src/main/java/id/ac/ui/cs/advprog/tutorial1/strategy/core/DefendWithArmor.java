package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    public String defend(){
        return "Defending with Armor : CLAANKKK!!!";
    }

    public String getType(){
        return "Armor";
    }
}
