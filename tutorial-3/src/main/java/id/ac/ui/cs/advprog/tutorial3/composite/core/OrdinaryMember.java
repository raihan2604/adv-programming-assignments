package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {
    String name;
    String role;
    ArrayList<Member> members;

    public OrdinaryMember(String nama, String role){
        this.name = nama;
        this.role = role;
        this.members = new ArrayList<Member>();
    }
        //TODO: Complete me
    public String getName(){
        return this.name;
    }

    public String getRole(){
        return this.role;
    }

    @Override
    public void addChildMember(Member member) {

    }

    @Override
    public void removeChildMember(Member member) {

    }

    public List<Member> getChildMembers(){
        return this.members;
    }
}
