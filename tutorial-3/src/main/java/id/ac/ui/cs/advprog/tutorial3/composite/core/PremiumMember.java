package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    String name;
    String role;
    ArrayList<Member> members;

    public PremiumMember(String nama, String roll){
        this.name = nama;
        this.role = roll;
        members = new ArrayList<Member>();
    }

    public String getName(){
        return this.name;
    }

    public String getRole(){
        return this.role;
    }

    public void addChildMember(Member member){
        if(this.role.equals("Master")) {
            this.members.add(member);
        }else{
            if(this.members.size()<3){
                this.members.add(member);
            }
        }
    }

    public void removeChildMember(Member member){
        this.members.remove(member);
    }

    @Override
    public List<Member> getChildMembers() {
        return this.members;
    }
//TODO: Complete me
}
