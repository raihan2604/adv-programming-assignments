package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class MagicUpgrade extends Weapon {

    Weapon weapon;
    int tambah;

    public MagicUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 15-20 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        Random r = new Random();
        tambah = r.nextInt(5)+15;
        return weapon.getWeaponValue()+tambah;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return weapon.getDescription()+" + Magic Upgrade";
    }
}
