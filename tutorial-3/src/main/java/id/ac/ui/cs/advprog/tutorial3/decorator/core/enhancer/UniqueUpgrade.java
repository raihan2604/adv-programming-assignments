package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;
//import org.graalvm.compiler.hotspot.nodes.PatchReturnAddressNode;

import java.util.Random;

public class UniqueUpgrade extends Weapon {

    Weapon weapon;
    int tambah;

    public UniqueUpgrade(Weapon weapon){

        this.weapon = weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }


    // Senjata bisa dienhance hingga 10-15 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        Random r = new Random();
        tambah = r.nextInt(5)+10;
        return weapon.getWeaponValue()+tambah;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return weapon.getDescription()+" + Unique Upgrade";
    }
}
