package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrdinaryMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new OrdinaryMember("Asuna", "Waifu");
    }

    @Test
    public void testMethodGetName() {
        //TODO: Complete me
        assertEquals("Asuna", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("Waifu", member.getRole());
    }

    @Test
    public void testMethodAddRemoveChildMemberDoNothing() {
        //TODO: Complete me
        Member a = new OrdinaryMember("a", "ngapain");
        member.addChildMember(a);
        assertEquals(0,member.getChildMembers().size());
        member.removeChildMember(a);
        assertEquals(0,member.getChildMembers().size());
    }
}
