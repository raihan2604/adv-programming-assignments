package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Aqua", "Goddess");
    }

    @Test
    public void testMethodGetName()
    {
        assertEquals("Aqua", member.getName());
        //TODO: Complete me
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("Goddess", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member a = new OrdinaryMember("a", "ngapain");
        member.addChildMember(a);
        assertEquals(1,member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member a = new OrdinaryMember("a", "ngapain");
        member.addChildMember(a);
        member.removeChildMember(a);
        assertEquals(0,member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member a = new OrdinaryMember("a", "ngapain");
        Member b = new OrdinaryMember("b", "ngapain");
        Member c = new OrdinaryMember("c", "ngapain");
        Member d = new OrdinaryMember("d", "ngapain");
        member.addChildMember(a);
        member.addChildMember(b);
        member.addChildMember(c);
        member.addChildMember(d);
        assertEquals(3,member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member e = new PremiumMember("e", "Master");
        Member a = new OrdinaryMember("a", "ngapain");
        Member b = new OrdinaryMember("b", "ngapain");
        Member c = new OrdinaryMember("c", "ngapain");
        Member d = new OrdinaryMember("d", "ngapain");
        e.addChildMember(a);
        e.addChildMember(b);
        e.addChildMember(c);
        e.addChildMember(d);
        assertEquals(4, e.getChildMembers().size());
    }
}
